FROM debian

# Set up dependencies
RUN apt-get update && apt-get install -y wget \
    ocaml \ 
    ocaml-native-compilers \ 
    make \
    gcc \ 
    g++ \
    git \
    unzip \
    perl \
    texlive-latex-base \
    procps \
    z3 \
    cvc3 \
    openjdk-8-jre-headless \
    zlib1g-dev

RUN wget "https://tla.msr-inria.inria.fr/tlaps/dist/current/tlaps-1.4.3.tar.gz" -O /tmp/tlaps-1.4.3.tar.gz
RUN wget "https://isabelle.in.tum.de/dist/Isabelle2018_app.tar.gz" -O /tmp/Isabelle2018.tar.gz
RUN wget "https://github.com/polyml/polyml/archive/v5.7.1.tar.gz" -O /tmp/polyml-v5.7.1.tar.gz
RUN wget "https://github.com/tlaplus/tlaplus/releases/download/v1.5.7/tla.zip" -O /tmp/tla.zip
RUN wget "http://cgi.csc.liv.ac.uk/~konev/software/trp++/translator/translate.tar.bz2" -O /tmp/translate.tar.bz2

# Set up TLA+:
WORKDIR /tmp/
RUN unzip tla.zip -d /opt/
ENV CLASSPATH="${CLASSPATH}:/opt/tla/"

# Set up PolyML
WORKDIR /tmp/
RUN tar xzf polyml-v5.7.1.tar.gz -C /usr/local
WORKDIR /usr/local/polyml-5.7.1/
RUN ./configure && make && make install

# Set up Isabelle/HOL:
WORKDIR /tmp/
RUN tar xzf Isabelle2018.tar.gz -C /usr/local
WORKDIR /usr/local/Isabelle2018/
ENV PATH="${PATH}:/usr/local/Isabelle2018/bin/"

# Make SPASS accessible to TLAPM:
ENV PATH="${PATH}:/usr/local/Isabelle2018/contrib/spass-3.8ds-1/x86_64-linux"

# TLAPS expects an executable named "isabelle-process", but Isabelle 2018 has it as a separate command:
RUN echo -e '#!/bin/sh\nexec /usr/local/Isabelle2018/bin/isabelle process "$@"' > /usr/local/Isabelle2018/bin/isabelle-process
RUN chmod +x /usr/local/Isabelle2018/bin/isabelle-process

# Set up Zenon:
WORKDIR /tmp/
RUN mkdir -p /opt/tlaps-1.4.3/
RUN tar xzf tlaps-1.4.3.tar.gz -C /opt/
WORKDIR /opt/tlaps-1.4.3/zenon/
RUN ./configure && make && make install

# Set up LS4 temporal prover:
WORKDIR /tmp/
RUN git clone https://github.com/quickbeam123/ls4.git
WORKDIR /tmp/ls4/core

# Remove prebuilt version of aiger,
RUN rm aiger.o aiger.o_32
# and build it ourselves:
RUN gcc -c aiger.c -o aiger.o

RUN make
RUN mkdir -p /opt/ls4/bin
ENV PATH="${PATH}:/opt/ls4/bin/"

RUN cp ls4 /opt/ls4/bin/ls4

# Set up the translation tool we need to use LS4:
WORKDIR /tmp/
RUN tar xvfj translate.tar.bz2 
WORKDIR /tmp/translate
RUN ./build.sh
RUN cp translate /opt/ls4/bin/ptl_to_trp

# Set up TLAPM:
WORKDIR /opt/tlaps-1.4.3/tlapm/
RUN ./configure && make all && make install